import {ApolloServer} from "@apollo/server";
import {startStandaloneServer} from '@apollo/server/standalone';
import {ApolloGateway, IntrospectAndCompose} from "@apollo/gateway";

const gateway = new ApolloGateway({
    supergraphSdl: new IntrospectAndCompose({
        subgraphs: [
            { name: "vendor", url: "http://localhost:4001/graphql" },
            { name: "item", url: "http://localhost:4002/graphql" },
            { name: "stores", url: "http://localhost:5066/graphql" }, // dotnet project
        ],
    }),
});

const server = new ApolloServer({ gateway });

// Note the top-level await!
const { url } = await startStandaloneServer(server, {listen: {port: 4000}});
console.log(`🚀  Server ready at ${url}`);