import {ApolloServer} from '@apollo/server';
import {startStandaloneServer} from '@apollo/server/standalone';
import {gql} from 'graphql-tag';
import {buildSubgraphSchema} from '@apollo/subgraph';

const typeDefs = gql`
    type Vendor @key(fields: "id") {
        id: ID! @external
        approvedItems: [Item!]!
    }

    

    
    type Item @key(fields: "id") {
        id: ID!
        title: String
    }

    type Query {
        items: [Item!]!
    }
`;

const resolvers = {
    Query: {
        items() {
            return items;
        }
    },
    Item: {
        __resolveReference(item, {}) {
            console.log('ITEM HERE');
            return items.filter(it => it.id === item.id)[0];
        }
    },
    Vendor: {
        approvedItems: (vendor) => {
            return items.filter(it => vendor.id % it.id === 0);
        },
    }
};

const items = [
    {id: "1", title: "Chips"},
    {id: "2", title: "Skittles"},
    {id: "3", title: "Reeses"},
    {id: "4", title: "Snickers"},
];

const server = new ApolloServer({
    schema: buildSubgraphSchema([{typeDefs, resolvers}])
});

// Note the top-level await!
const {url} = await startStandaloneServer(server, {listen: {port: 4002}});
console.log(`🚀  Server ready at ${url}`);