import {ApolloServer} from '@apollo/server';
import {startStandaloneServer} from '@apollo/server/standalone';
import {gql} from 'graphql-tag';
import {buildSubgraphSchema} from '@apollo/subgraph';

const typeDefs = gql`
    type Store @key(fields: "storeNumber") {
        storeNumber: ID! @external
        approvedVendors: [Vendor!]!
    }
    type Item @key(fields: "id") {
        id: ID! @external
        vendor: Vendor
    }

    type Vendor @key(fields: "id") {
        id: ID!
        name: String!
    }

    type Query {
        vendorById(id: ID!): Vendor
        vendors: [Vendor!]!
    }
`;

const resolvers = {
    Query: {
        vendorById: (parent, args) => {
            return vendors.filter(v => v.id === args.id)[0];
        },
        vendors: () => {
            return vendors;
        }
    },
    Vendor: {
        __resolveReference: (vendor) => {
            return vendors.filter(v => v.id === vendor.id)[0];
        }
    },
    Item: {
        vendor: (item) => {
            console.log(item);
            const found = vendors.filter(v => v.id === item.id);
            return found[0];
        }
    },
    Store: {
        approvedVendors: (store) => {
            return vendors.filter(it => it.id === store.storeNumber);
        }
    }
};

const vendors = [
    {id: "1", name: "Caseys"},
    {id: "2", name: "Red Bull"},
    {id: "3", name: "Coke"},
    {id: "4", name: "Nestlé"},
];

const server = new ApolloServer({
    schema: buildSubgraphSchema([{typeDefs, resolvers}])
});

// Note the top-level await!
const {url} = await startStandaloneServer(server, {listen: {port: 4001}});
console.log(`🚀  Server ready at ${url}`);